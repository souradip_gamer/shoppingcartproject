﻿using ShoppingCartProject.Models.Data;
using ShoppingCartProject.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ShoppingCartProject.Areas.Admin.Controllers
{
    public class PagesController : Controller
    {
        // GET: Admin/Pages
        public ActionResult Index()
        {
            List<PagesVM> pagesList;

            using(Db db = new Db())
            {
                pagesList = db.Pages.ToArray().OrderBy(x => x.Sorting).Select(x => new PagesVM(x)).ToList();
            }

            return View(pagesList);
        }

        public ActionResult AddPage()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AddPage(PagesVM model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

           using(Db db = new Db())
            {
                string slug;
                PagesDTO dto = new PagesDTO();
                if (string.IsNullOrWhiteSpace(model.Slug))
                {
                    slug = model.Title.Replace(" ", "-").ToLower();
                }
                else
                {
                    slug = model.Slug.Replace(" ", "-").ToLower();
                }

                if (db.Pages.Any(x => x.Title == model.Title) || db.Pages.Any(x => x.Slug == slug))
                {
                    ModelState.AddModelError("", "That title or slug already exists.");
                    return View(model);
                }


                dto.Title = model.Title;
                dto.Body = model.Body;
                dto.Slug = slug;
                dto.HasSidebar = model.HasSidebar;
                dto.Sorting = 100;

                db.Pages.Add(dto);

                db.SaveChanges();

               
            }

            TempData["SM"] = "Page added successgully";
            return View("AddPage");
        }

        public ActionResult EditPage(int id)
        {
            PagesVM model;

            using (Db db = new Db())
            {
                PagesDTO page = db.Pages.Find(id);

                if(page == null)
                {
                    return Content("Page not found");
                }

                model = new PagesVM(page);
       
            }
            return View(model);
        }

        [HttpPost]
        public ActionResult EditPage(PagesVM model)
        {
            if(!ModelState.IsValid)
            {
                return View(model);
            }
            using (Db db = new Db())
            {
                int id = model.Id;
                string slug = "home";

                PagesDTO dto = db.Pages.Find(id);
                dto.Title = model.Title;
                if(model.Slug != "home")
                {
                    if(string.IsNullOrWhiteSpace(model.Slug))
                    {
                        slug = model.Title.Replace(" ", "-").ToLower();
                    }
                    else
                    {
                        slug = model.Slug.Replace(" ", "-").ToLower();
                    }

                }

                if(db.Pages.Where(x=>x.Id != id).Any(x=>x.Title == model.Title) || (db.Pages.Where(x=>x.Id != id).Any(x=>x.Slug == slug)))
                {
                    ModelState.AddModelError("", "That title or slug already exists.");
                    return View(model);
                }

                dto.Sorting = 100;
                dto.Body = model.Body;
                dto.HasSidebar = model.HasSidebar;
                db.SaveChanges();

            }
            // Set TempData message
            TempData["SM"] = "You have edited the page!";

            // Redirect
            return RedirectToAction("EditPage");
        }

        public ActionResult PageDetails(int id)
        {
            PagesVM model;

            using (Db db = new Db())
            {
                PagesDTO page = db.Pages.Find(id);

                if(page == null)
                {
                    return Content("Page not found");
                }
                model = new PagesVM(page);
            }

                return View(model);
        }

        public ActionResult DeletePage(int id)
        {
            PagesVM model;

            using (Db db = new Db())
            {
                PagesDTO dto = db.Pages.Find(id);
                db.Pages.Remove(dto);
                db.SaveChanges();
            }
            return RedirectToAction("Index");
        }

        public ActionResult ReorderPages(int[] id)
        {
            int count = 1;
            using (Db db = new Db())
            {
                PagesDTO dto;
                foreach (var page in id)
                {
                    dto = db.Pages.Find(page);
                    dto.Sorting = 1;
                    db.SaveChanges();
                    count++;
                }
            }
                return View();
        }

        public ActionResult EditSidebar()
        {
            SidebarVM model;
            using (Db db = new Db())
            {
                SidebarDTO dto = db.Sidebar.Find(1);
                model = new SidebarVM(dto);
            }
                return View(model);
        }

        [HttpPost]
        public ActionResult EditSidebar(SidebarVM model)
        {
            using (Db db = new Db())
            {
                SidebarDTO dto = db.Sidebar.Find(1);
                dto.Body = model.Body;
                db.SaveChanges();

            }
            TempData["SM"] = "You have edited the sidebar!";
            return RedirectToAction("EditSidebar");
        }
    }
}